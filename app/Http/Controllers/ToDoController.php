<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ToDo;
use Illuminate\Support\Facades\DB;

class ToDoController extends Controller
{

    public function get_all_ToDo()
    {
        $todo = ToDo::all();
        return response()->json(['todo' => $todo]);
    }

    public function createToDo(Request $request)
    {
        $todo = new ToDo();
        $todo->title = $request->title;
        $todo->status = $request->status;
        $todo->save();

        $todoShow = ToDo::all();
        return response()->json(['todo' => $todoShow]);
    }

    public function updateToDo(Request $request)
    {
        $array = [
            'status' => $request->status
        ];
        DB::table('to_dos')
            ->where('id', $request->input('id'))
            ->update($array);

        $todoShow = ToDo::all();
        return response()->json(['todo' => $todoShow]);

    }

    public function deleteToDo(Request $request)
    {
        $todo = ToDo::find($request->input('id'));
        $todo->delete();
        $todoShow = ToDo::all();
        return response()->json(['todo' => $todoShow]);
    }

}
