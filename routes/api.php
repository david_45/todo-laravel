<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

# GET
Route::get('/gettodo', 'ToDoController@get_all_ToDo');

# POST
Route::post('/createtodo', 'ToDoController@createToDo');
Route::post('/updatetodo', 'ToDoController@updateToDo');
Route::post('/deletetodo', 'ToDoController@deleteToDo');